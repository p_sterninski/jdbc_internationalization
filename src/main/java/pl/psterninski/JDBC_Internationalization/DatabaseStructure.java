package pl.psterninski.JDBC_Internationalization;


/**
 * Klasa przechowuje istotne ustawienia dla bazy danych: nazwy i typy kolumn, adres URL bazy i nazwę sterownika.
 * Wartości te są potrzebne do utworzenia obiektu Database.
 * @author Przemysław Sterniński
 *
 */
public class DatabaseStructure {
	
	private final String[] columnNames = {"LOC", "COUNTRY", "DATE_START", "DATE_END", "PLACE", "COST", "CURRENCY"};
	private final String[] columnTypes = {"VARCHAR(5)", "VARCHAR(40)", "DATE", "DATE", "VARCHAR(20)", "DECIMAL(8,2)", "CHAR(3)"};
    private final String url = "jdbc:derby:memory:mydb;create=true";
    private final String driver = "org.apache.derby.jdbc.EmbeddedDriver";
    
    public DatabaseStructure(){}

	public String[] getColumnNames() {
		return columnNames;
	}

	public String[] getColumnTypes() {
		return columnTypes;
	}

	public String getUrl() {
		return url;
	}

	public String getDriver() {
		return driver;
	}
    
    
}
