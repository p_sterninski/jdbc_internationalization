package pl.psterninski.JDBC_Internationalization;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.function.Predicate;

/**
 * Obiekt TravelData zbiera inforamcje o ofertach z poszczególnych plików, następnie na podstawie metody 
 * getOffersDescriptionsList(Locale loc, String dateFormat) zwraca oferty w formie obeiktu List<String>, zgodnie z podaną
 * w parametrze lokalizacją i formatem daty.
 * @author Przemysław Sterniński
 *
 */
public class TravelData {
	
	List<String> offerList;
	
	public TravelData(File source){
		offerList = new ArrayList<String>();
		Predicate<Path> isFile = f-> Files.isRegularFile(f); 
		
		try {
			Files.walk(source.toPath())
					.filter(isFile)
					.forEach(file -> {
								try {
									Files.lines(file)
										.forEach(line -> offerList.add(line));
								} catch (IOException e) {
									System.out.println(file.getFileName() + " - cannot open the file.");
								}
							}
					);
		} catch (IOException e) {
			System.out.println("Cannot open files in directory: " + source.getAbsolutePath() + ".");
		}
		
	}
	
	
	
	public List<String> getOffersDescriptionsList(Locale loc, String dateFormat){
		Locale currLoc = loc;
		List<String> offerListFin = new ArrayList<>();
		String[] offerParts;
		
		for(String offer: getOfferList()){
			StringBuilder sb = new StringBuilder();
			offerParts = offer.split("\\t");
			
			
			String locString = offerParts[0];
			String country = offerParts[1];
			String dateStart = offerParts[2];
			String dateEnd = offerParts[3];
			String place = offerParts[4];
			String cost = offerParts[5];
			String currency = offerParts[6];
			
			Locale clientLoc = getLocale(locString);
			
			sb.append(loc.toString() + "\t");
			sb.append(getCountry(currLoc, clientLoc, country) + "\t"); 	//add country
			sb.append(getDate(dateFormat, dateStart) + "\t"); 			//add start date
			sb.append(getDate(dateFormat, dateEnd) + "\t"); 			//add end date
			sb.append(getPlace(currLoc, clientLoc, place) + "\t"); 		//add place
			sb.append(getValue(currLoc, clientLoc, cost) + "\t"); 		//add cost
			sb.append(currency); 										//add currency
			
			offerListFin.add(sb.toString());
			
		}
		
		return offerListFin;
	}
	
	
	public List<String> getOfferList(){
		return offerList;
	}
	
	
	private Locale getLocale(String localeString) {
		Locale locale = null;
		String[] locElements = localeString.split("_");
		
		if(locElements.length == 1){
			locale = new Locale(locElements[0]);
		}else if(locElements.length == 2){
			locale = new Locale(locElements[0], locElements[1]);
		}else if(locElements.length == 3){
			locale = new Locale(locElements[0], locElements[1], locElements[2]);
		}
		
		return locale;
	}
	
	
	private String getCountry(Locale currLoc, Locale clientLoc, String countryName) {
		Locale[] locTab = Locale.getAvailableLocales();
		
		for(Locale elLoc: locTab){
			if(elLoc.getDisplayCountry(clientLoc).equals(countryName)){
				return elLoc.getDisplayCountry(currLoc);
			}
		}
		
		return "[NoCountry]";
	}
	
	
	private String getDate(String dateFormat, String dateString) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		try {
			Date date = sdf.parse(dateString);
			sdf.applyPattern(dateFormat);
			return sdf.format(date);
		} catch (ParseException e) {
			System.out.println("Error during parsing a date from .properties file.");
		}
		
		return "[NoDate]";
	}
	
	
	private String getPlace(Locale currLoc, Locale clientLoc, String place) {
		ResourceBundle rbClient = ResourceBundle.getBundle("Place", clientLoc);
		ResourceBundle rbCurr = ResourceBundle.getBundle("Place", currLoc);
		
		for(String key: rbClient.keySet()){
			if(rbClient.getString(key).equals(place)){
				return rbCurr.getString(key);
			}
		}
		
		return "[NoPlace]";
	}
	
	
	private String getValue(Locale currLoc, Locale clientLoc, String value) {
		Number num;
		try {
			num = NumberFormat.getInstance(clientLoc).parse(value);
			return NumberFormat.getInstance(currLoc).format(num.doubleValue()); //dodano wartosc
		} catch (ParseException e) {
			System.out.println("Error during parsing a cost value from .properties file.");
		}
		
		return "[NoValue]";
	}
}
