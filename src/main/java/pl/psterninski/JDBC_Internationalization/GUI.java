package pl.psterninski.JDBC_Internationalization;

import java.awt.Dimension;
import java.util.List;
import java.util.Locale;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;

/**
 * Obiekt wyświtla zawartość plików ofert w wybranym przez użytkownika języku i ustawieniach regionalnych.
 * @author Przemysław Sterniński
 *
 */
public class GUI {

	private final String columnNames[] = {"LOC", "COUNTRY", "DATE_OFF", "DATE_BACK", "PLACE", "COST", "CURRENCY"};
	private TravelData travelData;
	
	public GUI(TravelData travelData) {
		this.travelData = travelData;
	}
	
public void show(){
	
	Locale loc = chooseLocale();
	
	List<String> offerList = travelData.getOffersDescriptionsList(loc, "yyyy-MM-dd");
	Object[][] tableData = new Object[offerList.size()][columnNames.length];
	
	for(int i = 0; i < tableData.length; i++){
		String[] offerParts = offerList.get(i).split("\\t");
		for(int j = 0; j < tableData[i].length; j++){
			tableData[i][j] = offerParts[j];
		}
	}
	
	JTable table = new JTable(tableData, columnNames);
	
	JFrame window = new JFrame("OfferList");
	window.setDefaultCloseOperation(3);
	
    JScrollPane scrollp = new JScrollPane(table);
    scrollp.setPreferredSize(new Dimension(600, 100));
    window.getContentPane().add(scrollp);

    window.pack();
    window.setLocationRelativeTo(null);
    window.setVisible(true);
}


public TravelData getTravelData() {
	return travelData;
}


public void setTravelData(TravelData travelData) {
	this.travelData = travelData;
}


private Locale chooseLocale(){
	String langCode = Locale.getDefault().getLanguage();
	String countryCode = Locale.getDefault().getCountry();
	
	String msg = "Enter language code";
	langCode = JOptionPane.showInputDialog(null, msg, langCode);
	
	msg = "Enter country code";
	countryCode = JOptionPane.showInputDialog(null, msg, countryCode);
	
	Locale loc = null;
	try {
		if(countryCode == null) {
			loc = new Locale(langCode);
		}else {
			loc = new Locale(langCode, countryCode);
		}
	}catch(NullPointerException e) {
		System.out.println("Entered language/country code is incorrect. Default values are used");
		loc = Locale.getDefault();
	}
	
	return loc;
	
 }
}