package pl.psterninski.JDBC_Internationalization;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.List;
import java.util.Locale;


/**
 * Klasa Database odpowiada za utworzenie tabeli w bazie, wypełnienie bazy danymi i wyświetlenie ichwyświetlenie.
 * @author Przemysław Sterniński
 *
 */
public class Database {
	
	private final TravelData travelData;
	private final String url;
	private final String driver;
	private final Locale locale;
	private final String[] columnNames;
	private final String[] columnTypes;
	
	public Database(TravelData travelData, Locale locale, DatabaseStructure dbStruct){
		this.travelData = travelData;
		this.locale = locale;
		this.url = dbStruct.getUrl();
		this.driver = dbStruct.getDriver();
		this.columnNames = dbStruct.getColumnNames();
		this.columnTypes = dbStruct.getColumnTypes();
	}
	
	
	public void create(){
		try {
			Class.forName(driver);
		} catch (ClassNotFoundException e) {
			System.out.println("\nCannot find database driver.");
			return;
		}
		
		List<String> offerList = travelData.getOffersDescriptionsList(locale, "yyyy-MM-dd");;
		
		try (
			Connection con = DriverManager.getConnection(url);
			Statement st = con.createStatement();
		){
			
			try{
				st.executeUpdate("DROP TABLE OFFER");
			}catch (SQLException e) {
				if(!e.getSQLState().equals("42Y55")){	//ten warunek nie podnosi wyjatku jesli tabela nie istnieje jeszcze w bazie, 
					throw e;							//w przciwnym razie rzucany jest wyjatek, ktory powinien byc obsluzony w kolejnym bloku catch
				}
			}
			st.executeUpdate("CREATE TABLE OFFER ("
							+ columnNames[0] + " " + columnTypes[0] + ","
							+ columnNames[1] + " " + columnTypes[1] + ","
							+ columnNames[2] + " " + columnTypes[2] + ","
							+ columnNames[3] + " " + columnTypes[3] + ","
							+ columnNames[4] + " " + columnTypes[4] + ","
							+ columnNames[5] + " " + columnTypes[5] + ","
							+ columnNames[6] + " " + columnTypes[6]
							+ ")");
			
			
			//insert values from offerList
			for(String offer: offerList){
				String[] offerTab = offer.split("\\t");
				insertValues(offerTab);
			}
			
			
			//show values from database
			System.out.println("\nContent of Database (from SELECT statement):");
			printValues(columnNames);

		} catch (SQLException exc) {
		     System.out.println("SQL except.: " + exc.getMessage());  // komunikat
		     System.out.println("SQL state  : " + exc.getSQLState()); // kod standardowy
		     System.out.println("Vendor errc: " + exc.getErrorCode()); // kod zależny od RDBMS
		}
		
		
	}
	
	
	public void insertValues(String[] offerTab) {
		System.out.print("\nInserting data to database...");
		try(
			Connection con = DriverManager.getConnection(url);
			PreparedStatement pst = con.prepareStatement("INSERT INTO OFFER ("
					+ columnNames[0] +", "
					+ columnNames[1] +", "
					+ columnNames[2] +", "
					+ columnNames[3] +", "
					+ columnNames[4] +", "
					+ columnNames[5] +", "
					+ columnNames[6] + ")"
					+ " VALUES (?, ?, ?, ?, ?, ?, ?)");
		){
			for(int i = 0; i < offerTab.length; i++) {
				if(i == 5) {
					try {
						pst.setDouble(i+1, NumberFormat.getInstance(locale).parse(offerTab[i]).doubleValue());
					} catch (ParseException e) {
						System.out.println("Wrong value in COST column. Data was not updated in database");
						return;
					}
				} else {
					pst.setString(i+1, offerTab[i]);
				}
			}
			pst.executeUpdate();
		
		} catch (SQLException e) {
			System.out.println("Blad podczas zapisu danych do bazy");
			return;
		}
		
		System.out.println("Completed successfully");
	}
	
	
	public void printValues(String[] columnNamesToPrint) {
		StringBuilder columnLine = new StringBuilder(columnNamesToPrint[0]);
		System.out.print(columnNamesToPrint[0] + "\t");
		for(int i = 1; i < columnNamesToPrint.length; i++) {
			columnLine.append(", " + columnNamesToPrint[i]);
			System.out.print(columnNamesToPrint[i] + "\t");
		}
		System.out.println();
		
		try(
			Connection con = DriverManager.getConnection(url);
			Statement st = con.createStatement();
			ResultSet rs = st.executeQuery("SELECT " + columnLine + " FROM OFFER");
		){
			while(rs.next()) {
				for(int i = 0; i < columnNamesToPrint.length; i++) {
					System.out.print(rs.getString(columnNamesToPrint[i]) + "\t");
				}
				System.out.println();
			}

		} catch (SQLException e) {
			System.out.println("Blad podczas odczytu danych z bazy");
		}
			
	}


	public String getUrl() {
		return url;
	}


	public String getDriver() {
		return driver;
	}


	public String[] getColumnNames() {
		return columnNames;
	}


	public String[] getColumnTypes() {
		return columnTypes;
	}

}
