package pl.psterninski.JDBC_Internationalization;


import java.io.*;
import java.util.*;

/**
 * Program wpisuje dane z plików z ofertami podróży, uzyskanymi od kontrahentów, do bazy danych biura podróży (używająć JDBC). 
 * Oferty mogą być w opisane stosując inną lokalizację (klasa Locale), ich wartość (wartość obiektu Locale) jest pierwszym elementem w ofercie.
 * Plik oferty składa się z:
 * - lokalizacja - napis,  oznaczający język_kraj
 * - kraj - nazwa kraju w języku kontrahenta
 * - data wyjazdu
 * - data powrotu 
 * - miejsce - jedno z: morze, jezioro, góry - w języku kontrahenta,
 * - cena - liczba w formacie liczb, używanym w kraji kontrahenta,
 * - symbol_waluty - PLN, USD, EUR
 * 
 * Oferty zpisywane są do bazy w jednej lokalizacji, podanej w konstruktorze obiektu Database. Miejsce wypoczynku (morze, jezioro, góry)
 * może zostac przetłumaczone w trzech językach: polskim, angielskim i niemickim, na podstawie plików .properties, wykorzystywanych przez
 * klasę ResourceBundle.
 * 
 * Na poczatku program wyświetla zawartość ofert w trzech lokalizacjach (pl_PL, en_GB, de_DE).
 * Następnie tworzona jest baza danych, w której umieszane są oferty, a następnie zawartość jest wyświetlana (z bazy danych).
 * Na koniec pokazują się okna z opcją wyboru języka i kraju (według kodu podawanego w konstruktore Locale, np. język = "pl", kraj = "PL") 
 * i wyświetla się tabela z danymi zgodnie z wybranymi ustawieniami.
 * 
 * @author Przemysław Sterniński
 *
 */
public class Main {

  public static void main(String[] args) {
	File dataDir = new File("src/main/resources/data");
    TravelData travelData = new TravelData(dataDir);
    
    //print data in three Locale (pl_PL, en_GB, de_DE)
    String dateFormat = "yyyy-MM-dd";
    showOffers(travelData, dateFormat, new Locale("pl", "PL"), new Locale("en", "GB"), new Locale("de", "DE"));
    
    //create database with data from offer files and print it out (from database, using SQL Statemnt)
    Database db = new Database(travelData, new Locale("en", "GB"), new DatabaseStructure());
    db.create();
    
    //show GUI with option to choose language and country (using codes like in Locale constructor), 
    //then table with output (in chosen Locale) shows up 
    GUI gui = new GUI(travelData);
    gui.show();
  }
  
  
  public static void showOffers(TravelData travelData, String dateFormat, Locale... tabLocale) {
	System.out.println("Contents of offer files, depending on locale:");
	for (Locale locale : Arrays.asList(tabLocale)) {
		System.out.println(locale + ":");
		List<String> offerList = travelData.getOffersDescriptionsList(locale, dateFormat);
		for (String offer : offerList) {
			System.out.println("  " + offer);
		}
	}
  }

}
